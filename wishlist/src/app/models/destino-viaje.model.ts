import {v4 as uuid} from 'uuid';

export class DestinoViaje {
	//nombre:string;
	//imagenUrl:string;
	selected: boolean;
	servicios: string[];
	id = uuid();

	constructor(public nombre: string, public imagenUrl: string) {
		//this.nombre = n;
		//this.imagenUrl = u;
		this.servicios = ['Piscina', 'Desayuno'];
		console.log('contructor');
	}

	setSelected(s: boolean) {
		this.selected = s;
	}

	isSelected(): boolean{
		return this.selected;
	}
}
